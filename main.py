from tkinter import *
from tkinter import filedialog
import ciphers.des3 as des3
import ciphers.aes as aes


root = Tk()
root.title('Ciphers')

cipvar = StringVar(root)
cipchoices = {
    'AES'
}
cipvar.set('AES')
cipmenu = OptionMenu(root, cipvar, *cipchoices)


modevar = StringVar(root)
modechoices = {
    'ecb',
    'cbc',
    'cfb',
    'ofb',
    'ctr'
}
modevar.set('ecb')
modemenu = OptionMenu(root, modevar, *modechoices)

typ = IntVar()
typ.set(0)


def getfile():
    root.filename = filedialog.askopenfilename(initialdir='/', title="Select file")
    Label(root, text=root.filename).grid(row=9, column=1, columnspan=3)
    return root.filename


def openkey():
    root.key = filedialog.askopenfilename(initialdir='/', title="Select file")
    Label(root, text=root.key).grid(row=5, column=1, columnspan=3)
    return root.key


def getkey():
    global typ
    if typ.get() == 0:
        return open(root.key, 'rb').read()
    if typ.get() == 1:
        return key.get()


def openfile():
    f = open(root.filename, 'rb')
    plain = f.read()
    f.close()
    return plain


def enc():
    global typ
    cip = cipvar.get()
    if cip == 'DES3':
        enc = des3.des3_encrypt(key=getkey(), mode=modevar.get(), plain=bytes(openfile()))
        with open('enc', 'wb+') as fd:
            fd.write(enc)
    if cip == 'AES':
        enc = aes.aes_encrypt(key=getkey(), mode=modevar.get(), msg=bytes(openfile()))
        with open('enc', 'wb+') as fd:
            fd.write(enc)


def dec():
    cip = cipvar.get()
    if cip == 'DES3':
        dec = des3.des3_decrypt(key=getkey(), ciphertext=bytes(openfile()), mode=modevar.get())
        with open('dec', 'wb+') as fd:
            fd.write(dec)
    if cip == 'AES':
        dec = aes.aes_decrypt(key=getkey(), mode=modevar.get(), msg=bytes(openfile()))
        with open('dec', 'wb+') as fd:
            fd.write(dec)


def uns(*args):

    if typ.get() == 0:
        key.config(state=DISABLED)
    elif typ.get() == 1:
        key.config(state=NORMAL)



Label(root, text='Choose a cipher').grid(row=1, column=1)
cipmenu.grid(row=2, column=1)

Label(root, text='Choose mode').grid(row=1, column=2)
modemenu.grid(row=2, column=2)

Button(root, text='Generate key', command=aes.keygen).grid(row=4, column=1)
opnk = Button(root, text='Open', command=openkey).grid(row=4, column=2)

cb = Checkbutton(root, text='Type key from keyboard (not safe!)', variable=typ, command=uns)
cb.grid(row=6, column=1)


Label(root, text='Key:').grid(row=7, column=1)
key = Entry(root, state=DISABLED)
key.grid(row=7, column=2)


Label(root, text='Choose a file').grid(row=8, column=1)
opn = Button(root, text='Open', command=getfile).grid(row=8, column=2)

Button(root, text='Encrypt', command=enc).grid(row=11, column=1)
Button(root, text='Decrypt', command=dec).grid(row=11, column=2)

typ.trace('w', uns)

root.mainloop()

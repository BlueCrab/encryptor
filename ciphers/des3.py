from Crypto.Cipher import DES3
from Crypto import Random
from Crypto.Util import Counter
import binascii


def des3_encrypt(key, mode, plain):

    fplain = plain.zfill(len(plain) + 8 - (len(plain) % 8))
    key = key.zfill(len(key) + 24 - (len(key) % 24))
    if mode == 'ecb':
        ciph = DES3.new(key, DES3.MODE_ECB)
        msg = ciph.encrypt(fplain)
        hexrep = binascii.hexlify(str(msg).encode('utf-8'))
        print(bin(int(hexrep, base=16))[2:])
        return msg
    elif mode == 'cbc':
        iv = Random.new().read(DES3.block_size)
        ciph = DES3.new(key, DES3.MODE_CBC, iv)
        msg = iv + ciph.encrypt(fplain)
        return msg
    elif mode == 'cfb':
        iv = Random.new().read(DES3.block_size)
        ciph = DES3.new(key, DES3.MODE_CFB, iv)
        msg = iv + ciph.encrypt(fplain)
        return msg
    elif mode == 'ofb':
        iv = Random.new().read(DES3.block_size)
        ciph = DES3.new(key, DES3.MODE_OFB, iv)
        msg = iv + ciph.encrypt(fplain)
        return msg
    elif mode == 'ctr':
        nonce = Random.new().read(int(DES3.block_size / 2))
        ctr = Counter.new(int(DES3.block_size * 8 / 2), prefix=nonce)
        ciph = DES3.new(key, DES3.MODE_CTR, counter=ctr)
        msg = nonce + ciph.encrypt(fplain)
        return msg


def des3_decrypt(key, mode, ciphertext):
    key = key.zfill(len(key) + 24 - (len(key) % 24))
    if mode == 'ecb':
        ciph = DES3.new(key, DES3.MODE_ECB)
        msg = ciph.decrypt(ciphertext)
        return msg
    elif mode == 'cbc':
        iv = ciphertext[:8]
        ciph = DES3.new(key, DES3.MODE_CBC, iv)
        print('DEC!')
        return ciph.decrypt(ciphertext[8:])
    elif mode == 'cfb':
        iv = ciphertext[:8]
        ciph = DES3.new(key, DES3.MODE_CFB, iv)
        print('DEC!')
        return ciph.decrypt(ciphertext[8:])
    elif mode == 'ofb':
        iv = ciphertext[:8]
        ciph = DES3.new(key, DES3.MODE_OFB, iv)
        print('DEC!')
        return ciph.decrypt(ciphertext[8:])
    elif mode == 'ctr':
        nonce = ciphertext[:int(DES3.block_size/2)]
        ctr = Counter.new(int(DES3.block_size * 8 / 2), prefix=nonce)
        ciph = DES3.new(key, DES3.MODE_CTR, counter=ctr)
        print('DEC!')
        return ciph.decrypt(ciphertext[4:])

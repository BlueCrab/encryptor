from Crypto.Cipher import AES
from Crypto import Random
from Crypto.Util import Counter
import binascii
import os


def keygen():
    key = os.urandom(32)
    print(binascii.hexlify(key))
    with open('key', 'wb+') as f:
        f.write(key)
    return key

def aes_encrypt(key, mode, msg):
    mf = (len(msg) + 16 - (len(msg) % 16)) - len(msg)
    filling_string = os.urandom(mf)
    fmsg = msg + filling_string

    if mode == 'ecb':
        aes = AES.new(key, AES.MODE_ECB)
        msg = str(mf).zfill(2).encode('utf-8') + aes.encrypt(fmsg)
        hexrep = binascii.hexlify(str(msg).encode('utf-8'))
        print(bin(int(hexrep, base=16))[2:])
        return msg
    elif mode == 'cbc':
        iv = Random.new().read(AES.block_size)
        ciph = AES.new(key, AES.MODE_CBC, iv)
        msg = iv + str(mf).zfill(2).encode('utf-8') + ciph.encrypt(fmsg)
        print(str(mf).zfill(2).encode('utf-8'))
        print(msg)
        return msg
    elif mode == 'cfb':
        iv = Random.new().read(AES.block_size)
        ciph = AES.new(key, AES.MODE_CFB, iv)
        msg = iv + str(mf).zfill(2).encode('utf-8') + ciph.encrypt(fmsg)
        print(str(mf).zfill(2).encode('utf-8'))
        print(msg)
        return msg
    elif mode == 'ofb':
        iv = Random.new().read(AES.block_size)
        ciph = AES.new(key, AES.MODE_OFB, iv)
        msg = iv + str(mf).zfill(2).encode('utf-8') + ciph.encrypt(fmsg)
        print(str(mf).zfill(2).encode('utf-8'))
        print(msg)
        return msg
    elif mode == 'ctr':
        nonce = Random.new().read(int(AES.block_size / 2))
        ctr = Counter.new(int(AES.block_size * 8 / 2), prefix=nonce)
        ciph = AES.new(key, AES.MODE_CTR, counter=ctr)
        print(str(mf).zfill(2).encode('utf-8'))
        msg = nonce + str(mf).zfill(2).encode('utf-8') + ciph.encrypt(fmsg)
        return msg


def aes_decrypt(key, mode, msg):
    # fmsg = msg.zfill(len(msg) + 16 - (len(msg) % 16))

    if mode == 'ecb':
        aes = AES.new(key, AES.MODE_ECB)
        dec_msg = aes.decrypt(msg[2:])
        hexrep = binascii.hexlify(str(dec_msg).encode('utf-8'))
        print(bin(int(hexrep, base=16))[2:])
        mi = int(msg[:2].decode('utf-8'))
        return dec_msg[:-mi]
    elif mode == 'cbc':
        iv = msg[:16]
        ciph = AES.new(key, AES.MODE_CBC, iv)
        dec = ciph.decrypt(msg[18:])
        mi = int(msg[16:18].decode('utf-8'))
        print('mi: ', mi)
        print('DEC!')
        return dec[:-mi]
    elif mode == 'cfb':
        iv = msg[:16]
        ciph = AES.new(key, AES.MODE_CFB, iv)
        dec = ciph.decrypt(msg[18:])
        mi = int(msg[16:18].decode('utf-8'))
        print('mi: ', mi)
        print('DEC!')
        return dec[:-mi]
    elif mode == 'ofb':
        iv = msg[:16]
        ciph = AES.new(key, AES.MODE_OFB, iv)
        dec = ciph.decrypt(msg[18:])
        mi = int(msg[16:18].decode('utf-8'))
        print('mi: ', mi)
        print('DEC!')
        return dec[:-mi]
    elif mode == 'ctr':
        nonce = msg[:int(AES.block_size/2)]
        ctr = Counter.new(int(AES.block_size * 8 / 2), prefix=nonce)
        ciph = AES.new(key, AES.MODE_CTR, counter=ctr)
        print('DEC!')
        dec = ciph.decrypt(msg[10:])
        mi = int(msg[8:10].decode('utf-8'))
        print('mi: ', mi)
        return dec[:-mi]
